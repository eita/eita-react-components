import React from 'react'

import * as S from './styles'
import LazyImage from '../LazyImage/LazyImage'
import { truncate } from '../utils/functions'

/**
 * @function ContentBlockForGrid
 */
const ContentBlockForGrid = ({
	img,
	title,
	onClick,
	contentType,
	isFixedWidth = true,
	children
}) => {
	return (
		<S.Wrapper
			style={isFixedWidth ? { width: '100%', padding: '0 10px' } : {}}
			onClick={onClick}
			className="proteja-wrapper"
		>
			<S.Block className="proteja-block">
				{children && (
					<S.TopWrapper className="proteja-top-wrapper">
						<S.ContentTypeWrapper className="proteja-content-type-wrapper">
							{children}
							<S.Type className="proteja-content-type">
								{contentType}
							</S.Type>
						</S.ContentTypeWrapper>
					</S.TopWrapper>
				)}
				<LazyImage src={img} />
			</S.Block>
			<S.Title className="proteja-title">{truncate(title, 60)}</S.Title>
		</S.Wrapper>
	)
}

export default ContentBlockForGrid
