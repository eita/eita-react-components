import React from 'react'
import LazyImage from '../LazyImage/LazyImage'

import { truncate } from '../utils/functions'

import * as S from './styles'

/**
 * @function ContentBlock
 */
const ContentBlock = ({
	img,
	title,
	shortDescription,
	onClick,
	icon,
	contentType,
	children
}) => {
	return (
		<S.Wrapper>
			<S.Block className="img" onClick={onClick}>
				<LazyImage src={img} />
				{icon && (
					<S.Icon>
						{children}
						<S.Type className="content-type">{contentType}</S.Type>
					</S.Icon>
				)}
			</S.Block>
			<S.ContentInfoContainer>
				<S.Title onClick={onClick}>{truncate(title, 100)}</S.Title>
				<S.Description>{truncate(shortDescription, 290)}</S.Description>
				{/* <S.KeywordsContainer>
					{keywords &&
						keywords
							.split(', ')
							.map((keyword, idx) => (
								<S.Keyword key={idx}>{keyword}</S.Keyword>
							))}
				</S.KeywordsContainer> */}
			</S.ContentInfoContainer>
		</S.Wrapper>
	)
}

export default ContentBlock
