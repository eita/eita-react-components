export const truncate = (input, quantity) =>
	input?.length > quantity ? `${input.substring(0, quantity)}...` : input
