import React, { useEffect, useState } from 'react'
import { truncate } from '../utils/functions'

import * as S from './styles'
import Icon from '../Icon'
import theme from '../styles/theme'

const ContentDetail = ({
	category,
	item,
	setActive,
	isPage,
	nextItem,
	previousItem,
	contentTypeIcon,
	children
}) => {
	const [selectedItem, setSelectedItem] = useState(null)
	const [isSharingBarVisible, setIsSharingBarVisible] = useState(false)

	useEffect(() => {
		setSelectedItem(null)
	}, [item])

	useEffect(() => {
		function keyDownListener(e) {
			if (e.key === 'ArrowLeft') {
				previousItem()
			} else if (e.key === 'ArrowRight') {
				nextItem()
			}
		}
		document.addEventListener('keydown', keyDownListener)

		return () => {
			document.removeEventListener('keydown', keyDownListener)
		}
	}, [nextItem, previousItem])

	return (
		item &&
		category && (
			<S.Background onClick={setActive}>
				<S.IconContainer>
					<Icon type="far fa-times" onClick={setActive} />
					{/* <Icon
						type="far fa-arrow-left"
						onClick={(e) => {
							e.stopPropagation()
							previousItem()
						}}
					/>
					<Icon
						type="far fa-arrow-right"
						onClick={(e) => {
							e.stopPropagation()
							nextItem()
						}}
					/> */}
					<S.Detail
						isPage={isPage}
						onClick={(e) => {
							e.stopPropagation()
						}}
					>
						<S.Wrapper
							className={category.id === 'series' && 'is-serie'}
						>
							{item.content_type &&
								item.content_type.icon_name && (
									<S.TopWrapper>
										<S.ContentTypeWrapper>
											{contentTypeIcon}
											<S.Type>
												{item.content_type.name}
											</S.Type>
										</S.ContentTypeWrapper>
									</S.TopWrapper>
								)}
							{category.id !== 'series' && (
								<>
									{item && item.cover_id && (
										<S.Cover
											className="cover"
											style={{
												backgroundImage:
													"url('" +
													item.cover_id +
													"')"
											}}
										></S.Cover>
									)}
									{item && <ContentInfo item={item} />}
								</>
							)}

							{/* {category.id === 'series' && (
								<>
									{selectedItem && (
										<ContentInfo
											half
											item={selectedItem}
											handleSelect={setSelectedItem}
											isPage={isPage}
										/>
									)}
									{!selectedItem && (
										<S.InfoSerie>
											<h2>{item.name}</h2>
											<S.Cover
												className="cover"
												bg={item.cover_id}
											></S.Cover>
										</S.InfoSerie>
									)}
									<SeriesInfo
										handleSelect={setSelectedItem}
										selected={selectedItem}
										item={item}
									/>
								</>
							)} */}
						</S.Wrapper>
						<S.FileLink>
							<S.GoToContentButton
								target="_blank"
								href={'/conteudo/' + item.friendly_url}
							>
								Ver Mais
							</S.GoToContentButton>
							<S.SharingButton
								onClick={() =>
									setIsSharingBarVisible(
										(prevState) => !prevState
									)
								}
							>
								<Icon
									type="far fa-regular fa-paper-plane"
									style={{
										color: theme.colors.primary,
										fontSize: '16px'
									}}
								/>
								Compartilhar
							</S.SharingButton>

							{/* <ContentUserActions
								id={item.id}
								url={item.friendly_url}
								handleSelect={setSelectedItem}
								item={item}
							/> */}
						</S.FileLink>
						<S.SharingBar
							className={isSharingBarVisible && 'clicked'}
						>
							{children}
						</S.SharingBar>
					</S.Detail>
				</S.IconContainer>
			</S.Background>
		)
	)
}

const ContentInfo = ({ item }) => {
	return (
		<S.Overview>
			<S.Title>{item.title}</S.Title>
			<S.Info>
				<S.InfoWrapper>
					<S.InfoTitle>Autoria: </S.InfoTitle>
					<S.InfoText>
						{item.author ? item.author.replace(/[,]/g, ', ') : ''}
					</S.InfoText>
				</S.InfoWrapper>
			</S.Info>
			<S.Short>{truncate(item.short_description, 525)}</S.Short>
			{item.themes && (
				<S.Themes>
					{item.themes.map((t, idx) => (
						<S.ThemeChip key={idx}>{t.name}</S.ThemeChip>
					))}
				</S.Themes>
			)}
		</S.Overview>
	)
}

// const SeriesInfo = ({ item, selected, handleSelect }) => {
// 	return (
// 		<S.Episodes>
// 			<h2>Conteúdos da série</h2>
// 			<S.List>
// 				{item.content_indications.map((episode, i) => (
// 					<S.Item
// 						className={
// 							selected && selected.id === episode.id && 'selected'
// 						}
// 						key={i}
// 						onClick={() => handleSelect(episode)}
// 					>
// 						<S.Image img={episode.cover_id}></S.Image>
// 						<S.Description>
// 							<p className="title">{episode.title}</p>
// 							<br />
// 							<p>{truncate(episode.short_description, 200)}</p>
// 						</S.Description>
// 					</S.Item>
// 				))}
// 			</S.List>
// 		</S.Episodes>
// 	)
// }

export default ContentDetail
